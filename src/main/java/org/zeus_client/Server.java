package org.zeus_client;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.iki.elonen.NanoHTTPD;
import org.zeus_client.delayers.AccessTokenDelayer;
import org.zeus_client.delayers.CalendarDelayer;
import org.zeus_client.delayers.GroupsListDelayer;
import org.zeus_client.structures.Group;

import java.util.Arrays;
import java.util.HashMap;

public class Server extends NanoHTTPD {
    private final AccessTokenDelayer accessTokenHolder;
    private final GroupsListDelayer groupsListDelayer;
    private final HashMap<Integer, CalendarDelayer> calendarsCache = new HashMap<>();

    public Server(String email, String password, int port) throws Exception {
        super(port);
        this.accessTokenHolder = new AccessTokenDelayer(email, password);
        this.groupsListDelayer = new GroupsListDelayer(accessTokenHolder);

        System.out.println("Start server on " + port);
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
    }

    @Override
    public Response serve(IHTTPSession session) {

        if (session.getUri().matches("^/ics/\\d+$")) {
            return this.serveICS(Integer.parseInt(session.getUri().split("/")[2]));
        } else if (session.getUri().equals("/groups"))
            return this.serveGroupsList();

        return newFixedLengthResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "Not Found");
    }

    private Response serveGroupsList() {
        try {
            return newFixedLengthResponse(Response.Status.OK, "application/json", this.groupsListDelayer.get());
        } catch (Exception e) {
            e.printStackTrace();
            return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Fetch failed");
        }
    }

    private Response serveICS(int id) {
        try {
            // Check if group id is correct
            var res = new ObjectMapper().readValue(this.groupsListDelayer.get(), Group[].class);

            if (Arrays.stream(res).noneMatch(f -> f.getId() == id))
                return newFixedLengthResponse(Response.Status.NOT_FOUND, "text/plain", "Calendar not found!");


            if (!calendarsCache.containsKey(id))
                calendarsCache.put(id, new CalendarDelayer(id, accessTokenHolder));

            return newFixedLengthResponse(Response.Status.OK, "text/calendar", calendarsCache.get(id).get());
        } catch (Exception e) {
            e.printStackTrace();
            return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Fetch failed");
        }
    }
}
