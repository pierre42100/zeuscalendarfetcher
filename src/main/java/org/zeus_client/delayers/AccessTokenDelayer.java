package org.zeus_client.delayers;

import org.zeus_client.TokenGrabber;

public class AccessTokenDelayer extends Delayer {
    private final String email;
    private final String password;

    public AccessTokenDelayer(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    protected String fetch() throws Exception {
        return TokenGrabber.GrabToken(this.email, this.password);
    }

    @Override
    protected long duration() {
        return 3600;
    }
}
