package org.zeus_client.delayers;

import org.zeus_client.CalendarFetcher;

public class CalendarDelayer extends Delayer {
    private final int calendarID;
    private final AccessTokenDelayer tokenDelayer;

    public CalendarDelayer(int calendarID, AccessTokenDelayer tokenDelayer) {
        this.calendarID = calendarID;
        this.tokenDelayer = tokenDelayer;
    }

    @Override
    protected String fetch() throws Exception {
        System.out.println("Fetch calendar " + calendarID);

        return CalendarFetcher.getCalendar(tokenDelayer.get(), calendarID);
    }

    @Override
    protected long duration() {
        return 120;
    }
}
