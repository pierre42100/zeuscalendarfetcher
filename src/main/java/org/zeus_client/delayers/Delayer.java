package org.zeus_client.delayers;

import java.time.Instant;

public abstract class Delayer {

    private long lastFetched = 0;
    private String data = null;

    protected abstract String fetch() throws Exception;

    protected abstract long duration();


    private static long time() {
        return Instant.now().getEpochSecond();
    }

    public String get() throws Exception {
        if (lastFetched + duration() > time())
            return data;

        data = fetch();
        lastFetched = time();

        return data;
    }
}
