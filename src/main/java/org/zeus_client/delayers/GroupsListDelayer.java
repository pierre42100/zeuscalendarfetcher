package org.zeus_client.delayers;

import org.zeus_client.CalendarFetcher;

public class GroupsListDelayer extends Delayer {
    private final AccessTokenDelayer accessTokenDelayer;

    public GroupsListDelayer(AccessTokenDelayer accessTokenDelayer) {
        this.accessTokenDelayer = accessTokenDelayer;
    }


    @Override
    protected String fetch() throws Exception {
        return CalendarFetcher.getCalendarsList(accessTokenDelayer.get());
    }

    @Override
    protected long duration() {
        return 3600 * 24;
    }
}
