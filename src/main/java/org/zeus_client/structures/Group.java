package org.zeus_client.structures;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Group {
    private int id;

    public int getId() {
        return id;
    }
}
