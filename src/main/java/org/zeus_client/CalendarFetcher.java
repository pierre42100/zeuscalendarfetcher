package org.zeus_client;

import java.net.HttpURLConnection;
import java.net.URL;

public class CalendarFetcher {
    public static String getCalendar(String token, int id) throws Exception {
        return getURL(token, Constants.ZEUS_URL + "/api/group/" + id + "/ics");
    }

    public static String getCalendarsList(String token) throws Exception {
        return getURL(token, Constants.ZEUS_URL + "/api/group");
    }

    private static String getURL(String token, String reqURL) throws Exception {
        URL url = new URL(reqURL);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        try {
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", "Bearer " + token);

            return new String(con.getInputStream().readAllBytes());
        } finally {
            con.disconnect();
        }
    }
}
