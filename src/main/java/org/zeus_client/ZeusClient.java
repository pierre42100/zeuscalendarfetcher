package org.zeus_client;

public class ZeusClient {
    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            System.err.println("Usage: [email] [password] [listen-port]");
            System.exit(-1);
        }

        new Server(args[0], args[1], Integer.parseInt(args[2]));
    }
}
