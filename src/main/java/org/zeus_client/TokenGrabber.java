package org.zeus_client;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.Augmenter;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.time.Duration;

public class TokenGrabber {

    public static String GrabToken(String email, String password) throws Exception {
        var options = new FirefoxOptions();
        options.setHeadless(true);

        WebDriver driver = new FirefoxDriver(options);
        try {
            driver.get(Constants.ZEUS_URL);

            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(1));

            // Find login button
            WebElement officeButton = driver.findElement(By.cssSelector("[src=\"assets/svg/office-button.svg\"]"));
            officeButton.click();


            while (driver.getCurrentUrl().startsWith(Constants.ZEUS_URL)) {
                Thread.sleep(100);
            }

            // Enter email
            WebElement mailInput = driver.findElement(By.cssSelector("input[type='email']"));
            mailInput.sendKeys(email);
            mailInput.sendKeys(Keys.RETURN);

            while (!driver.getCurrentUrl().startsWith("https://sts.epita.fr/")) {
                Thread.sleep(100);
            }

            // Enter password
            WebElement passwordInput = driver.findElement(By.cssSelector("input[type='password']"));
            passwordInput.sendKeys(password);
            passwordInput.sendKeys(Keys.RETURN);

            while (driver.getCurrentUrl().startsWith("https://sts.epita.fr/")) {
                Thread.sleep(100);
            }

            // Stay signed in
            WebElement noButton = driver.findElement(By.cssSelector("input.button"));
            noButton.click();


            while (!driver.getCurrentUrl().startsWith(Constants.ZEUS_URL)) {
                Thread.sleep(100);
            }

            // Retrieve token
            WebStorage webStorage = (WebStorage) new Augmenter().augment(driver);
            var storage = webStorage.getSessionStorage();

            return GetAppToken(storage.getItem("access_token"));

        } finally {
            driver.close();
        }
    }

    private static String GetAppToken(String accessToken) throws Exception {
        URL url = new URL(Constants.ZEUS_URL + "/api/User/OfficeLogin");
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) con;
        http.setRequestMethod("POST"); // PUT is another valid option
        http.setDoOutput(true);
        byte[] out = ("{\"accessToken\":\"" + accessToken + "\"}").getBytes(StandardCharsets.UTF_8);
        int length = out.length;

        http.setFixedLengthStreamingMode(length);
        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.connect();
        try (OutputStream os = http.getOutputStream()) {
            os.write(out);
        }

        var bytes = http.getInputStream().readAllBytes();

        return new String(bytes);
    }
}
